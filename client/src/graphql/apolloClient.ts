import {
  ApolloClient,
  HttpLink,
  InMemoryCache,
  Operation,
  split,
} from "@apollo/client";
import { GraphQLWsLink } from "@apollo/client/link/subscriptions";
import { createClient } from "graphql-ws";

import { getMainDefinition } from "@apollo/client/utilities";

const httpLink = new HttpLink({
  uri: process.env.REACT_APP_API_URL,
});

// const wsLink = new GraphQLWsLink(
//   createClient({
//     url: process.env.REACT_APP_WS_API_URL!,
//   })
// );

// const isSubscription = ({ query }: Operation) => {
//   const definition = getMainDefinition(query);
//   return (
//     definition.kind === "OperationDefinition" &&
//     definition.operation === "subscription"
//   );
// };

// const link = split(isSubscription, wsLink, httpLink);

const apolloClient = new ApolloClient({
  link: httpLink,
  cache: new InMemoryCache(),
});

export { apolloClient };
