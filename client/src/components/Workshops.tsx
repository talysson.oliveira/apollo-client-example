import { gql, useQuery, useSubscription } from "@apollo/client";

type Workshop = {
  id: number;
  name: string;
};

const GET_ALL_WORKSHOPS = gql`
  query GetAllWorkshops {
    workshops {
      id
      name
    }
  }
`;

type GetAllWorkshopsResponse = {
  workshops: Workshop[];
};

// const WORKSHOP_CREATED = gql`
//   subscription WorkshopCreated {
//     workshopCreated {
//       id
//       name
//     }
//   }
// `;

// type WorkshopCreatedData = {
//   workshopCreated: Workshop;
// };

const Workshops = () => {
  const { data: workshopsResponse, loading: workshopsLoading } =
    useQuery<GetAllWorkshopsResponse>(GET_ALL_WORKSHOPS);

  // useSubscription<WorkshopCreatedData>(WORKSHOP_CREATED, {
  //   onSubscriptionData({ subscriptionData }) {
  //     if (subscriptionData.error) {
  //       console.error(subscriptionData.error);
  //       return;
  //     }

  //     alert(
  //       `New workshop added: ${subscriptionData.data?.workshopCreated.name}`
  //     );
  //   },
  // });

  if (workshopsLoading) {
    return <strong>Loading</strong>;
  }

  return (
    <ul>
      {workshopsResponse?.workshops.map((workshop) => (
        <li key={workshop.id}>{workshop.name}</li>
      ))}
    </ul>
  );
};

export { Workshops };
