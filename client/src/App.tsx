import React from "react";
import { ApolloProvider } from "@apollo/client";
import { apolloClient } from "./graphql/apolloClient";
import { Workshops } from "./components/Workshops";

function App() {
  return (
    <ApolloProvider client={apolloClient}>
      <Workshops />
    </ApolloProvider>
  );
}

export default App;
