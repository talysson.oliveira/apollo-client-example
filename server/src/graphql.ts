import { PubSub } from "graphql-subscriptions";
import {
  makeSchema,
  objectType,
  inputObjectType,
  mutationField,
  nonNull,
  queryField,
  list,
  booleanArg,
  idArg,
  stringArg,
  subscriptionField,
} from "nexus";

const pubSub = new PubSub();

let id = 1;

const data = {
  workshops: [
    {
      id: id++,
      name: "Frontend",
      subjects: ["frontend", "javascript", "react", "css"],
      active: true,
    },
    {
      id: id++,
      name: "DDD42",
      subjects: ["ddd", "architecture", "testing"],
      active: false,
    },
    {
      id: id++,
      name: "Modern Java",
      subjects: ["java", "oop", "kotlin"],
      active: true,
    },
  ],
};

const Workshop = objectType({
  name: "Workshop",
  definition(t) {
    t.nonNull.id("id");
    t.nonNull.string("name");
    t.nonNull.list.string("subjects");
    t.nonNull.boolean("active");
  },
});

const GetWorkshops = queryField("workshops", {
  type: nonNull(list(nonNull(Workshop))),
  args: {
    includeInactives: booleanArg(),
  },
  resolve: (_, args) => {
    const includeInactives = args.includeInactives ?? false;

    if (includeInactives) {
      return data.workshops;
    }

    return data.workshops.filter((workshop) => workshop.active);
  },
});

const GetSubjects = queryField("subjects", {
  type: nonNull(list(nonNull("String"))),
  resolve: () =>
    Array.from(
      new Set(data.workshops.flatMap((workshop) => workshop.subjects))
    ),
});

const WorkshopProps = inputObjectType({
  name: "WorkshopProps",
  definition(t) {
    t.nonNull.string("name");
    t.nonNull.list.string("subjects");
  },
});

const CreateWorkshop = mutationField("createWorkshop", {
  type: nonNull(Workshop),
  args: {
    workshopProps: nonNull(WorkshopProps),
  },
  async resolve(_, args) {
    const newWorkshop = {
      id: id++,
      active: true,
      ...args.workshopProps,
    };

    data.workshops.push(newWorkshop);

    await pubSub.publish("workshopCreated", newWorkshop.id);

    return newWorkshop;
  },
});

const AddSubject = mutationField("addSubject", {
  type: nonNull(Workshop),
  args: {
    id: nonNull(idArg()),
    subject: nonNull(stringArg()),
  },
  resolve(_, args) {
    const workshop = data.workshops.find(
      (workshop) => workshop.id === Number(args.id)
    );

    workshop.subjects = Array.from(
      new Set(workshop.subjects).add(args.subject)
    );

    return workshop;
  },
});

const WorkshopCreated = subscriptionField("workshopCreated", {
  type: nonNull(Workshop),
  subscribe: () => pubSub.asyncIterator("workshopCreated"),
  resolve: (workshopId: number) => {
    const workshop = data.workshops.find(
      (workshop) => workshop.id === workshopId
    );

    return workshop;
  },
});

const schema = makeSchema({
  types: [
    GetWorkshops,
    GetSubjects,
    CreateWorkshop,
    AddSubject,
    WorkshopCreated,
  ],
  outputs: false,
});

export { schema };
