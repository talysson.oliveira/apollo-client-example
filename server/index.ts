import { schema } from "./src/graphql";
import { server } from "./src/server";

server(schema);
